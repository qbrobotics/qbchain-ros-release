## qb_chain (noetic) - 2.2.3-1

The packages in the `qb_chain` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_chain --track noetic --rosdistro noetic` on `Thu, 18 Nov 2021 16:25:19 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`
- `qb_chain_msgs`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-1`
- new version: `2.2.3-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_chain (melodic) - 2.2.3-1

The packages in the `qb_chain` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_chain --track melodic --rosdistro melodic` on `Thu, 18 Nov 2021 16:22:15 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`
- `qb_chain_msgs`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.2.2-1`
- new version: `2.2.3-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_chain (noetic) - 2.2.2-1

The packages in the `qb_chain` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_chain --track noetic --rosdistro noetic --new-track` on `Mon, 30 Aug 2021 10:45:00 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`
- `qb_chain_msgs`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_chain (melodic) - 2.2.2-1

The packages in the `qb_chain` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_chain --track melodic --rosdistro melodic` on `Mon, 30 Aug 2021 10:39:31 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`
- `qb_chain_msgs`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_chain (kinetic) - 2.1.1-1

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_chain --track kinetic --rosdistro kinetic` on `Wed, 12 Jun 2019 07:29:48 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `2.1.0-1`
- old version: `2.1.0-1`
- new version: `2.1.1-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## qb_chain (kinetic) - 2.1.0-1

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_chain --track kinetic --rosdistro kinetic` on `Tue, 28 May 2019 16:16:27 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_controllers`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.1.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## qb_chain (melodic) - 2.0.0-0

The packages in the `qb_chain` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_chain --track melodic --rosdistro melodic --new-track` on `Fri, 01 Jun 2018 14:07:22 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_chain (lunar) - 2.0.0-0

The packages in the `qb_chain` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_chain --track lunar --rosdistro lunar --new-track` on `Fri, 01 Jun 2018 13:59:49 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_chain (kinetic) - 2.0.0-0

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_chain --track kinetic --rosdistro kinetic` on `Fri, 01 Jun 2018 13:45:30 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `1.0.4-0`
- old version: `1.0.4-0`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_chain (kinetic) - 1.0.4-0

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_chain` on `Fri, 24 Nov 2017 11:47:38 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `1.0.3-0`
- old version: `1.0.3-0`
- new version: `1.0.4-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_chain (kinetic) - 1.0.3-0

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_chain` on `Fri, 23 Jun 2017 16:10:00 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbchain-ros-release.git
- rosdistro version: `1.0.1-0`
- old version: `1.0.1-0`
- new version: `1.0.3-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_chain (kinetic) - 1.0.1-0

The packages in the `qb_chain` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_chain --edit` on `Mon, 19 Jun 2017 16:02:47 -0000`

These packages were released:
- `qb_chain`
- `qb_chain_control`
- `qb_chain_description`

Version of package(s) in repository `qb_chain`:

- upstream repository: https://bitbucket.org/qbrobotics/qbchain-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


